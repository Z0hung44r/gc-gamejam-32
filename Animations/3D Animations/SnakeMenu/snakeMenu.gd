extends Spatial


enum ANIM {
	DEFEAT
	IDLE
	TPOSE
	WINNER

}

export(ANIM) var anim = ANIM.IDLE


onready var animation = get_node("AnimationPlayer")



# Called when the node enters the scene tree for the first time.
func _ready():
	var listAnimation = animation.get_animation_list()
	print(listAnimation)
	animation.play(listAnimation[anim])
	animation.get_animation(listAnimation[ANIM.WINNER]).loop = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
