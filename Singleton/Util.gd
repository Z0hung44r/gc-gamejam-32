extends Node

enum PointOfView {
	FPS,
	PLATFORMER,
	TOP_VIEW
}

var oldView = null
var currentView = PointOfView.FPS
var playerPosition = null
var nightVisionActive = false
var moveCamera = false
