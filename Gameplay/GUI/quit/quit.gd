extends TextureButton

onready var level1 = load("res://Scenes/Build/Level1.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	
	connect("button_down",self,"_on_start_click")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_start_click():
	get_tree().quit()
