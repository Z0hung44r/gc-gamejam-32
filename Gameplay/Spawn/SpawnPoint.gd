extends Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	$CSGSphere.visible = false
	var scene = load('res://Characters/Player/Player.tscn')
	var player = scene.instance()
	add_child(player)
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
