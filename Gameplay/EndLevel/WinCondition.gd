extends Area


onready var win = load("res://Scenes/Build/WinScene/WinScene.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var player = KinematicBody
	connect( "body_entered" ,self, "_body_entered")



func _body_entered(body):
	if body is Player:
		get_tree().change_scene_to(win)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
