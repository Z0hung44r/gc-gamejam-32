extends Camera

export(float) var minLookAngleV = -55.0 # Angle de vue verticale
export(float) var maxLookAngleV = 70.0 
export(float) var minLookAngleH = 10.0 # Angle de vue horizontale
export(float) var maxLookAngleH = 300.0 
export(float) var lookSensitivity = 0.1 # Sensibilité de la caméra en vue FPS

var cameraRotation : Vector2

func Move(delta):
	self.rotation_degrees.x = clamp(self.rotation_degrees.x, minLookAngleV, maxLookAngleV)
	
	self.rotation_degrees.x = cameraRotation.x
	self.rotation_degrees.y = cameraRotation.y

func _input(event):
	if event is InputEventMouseMotion and Util.currentView == Util.PointOfView.FPS and Util.moveCamera:
		cameraRotation.x -= event.relative.y * lookSensitivity
		cameraRotation.y -= event.relative.x * lookSensitivity

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	cameraRotation = Vector2(0, -180) # Oriente la caméra dans la bonne direction
