extends Node

# Un timer pour faire clignoter une lampe

export var delay = .1

var rng    = RandomNumberGenerator.new()
var timer  = 0
var active = true

func _ready():
#{	
	rng.randomize()
	active = rng.randf_range(0, 1) < .1 #10% des lampes clignoteront
	
	if active: Reset(false)
#}

func _process(delta):
#{
	if not active: 
		return
	
	if ProcessTime(delta):
		return
	
	Flick()
	Reset(Idle())
#}

#Ecoule le timer et renvoie true quand écoulé
func ProcessTime(delta) -> bool:
#{
	timer -= delta
	return timer > 0
#}

#Inverse l'état de self.visible
func Flick():
#{	
	self.visible = not self.visible
#}

#Remonte le timer et randomise le prochain delay
func Reset(idle):
#{
	if idle:
		timer = rng.randf_range(5, 10)
	else:
		timer = delay + rng.randf_range(-1, 1) * delay	
#}

#Chance de d'éviter le flicker
func Idle() -> bool:
#{
	return self.visible && rng.randf_range(0, 1) < .5
#}
