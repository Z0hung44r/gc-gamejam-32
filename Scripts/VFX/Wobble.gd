extends Node

# Vacillement d'une source de lumière

export var baseEnergy =   1
export var minEnergy  = .25
export var maxEnergy  =   1

export var baseRange  =  .5
export var minRange   =  .5
export var maxRange   = 1.5

export var speed : float = 1

var noise  = OpenSimplexNoise.new()
var sample = 0

func _ready():
#{	
	InitNoise()
#}

func _process(delta):
#{
	ComputeSample(delta)
	
	var sampled = SampleNoise()
	
	self.light_energy = ComputeEnergy(sampled)
	self.omni_range  = ComputeRange(sampled) 
#}

func ComputeRange(sampled) -> float:
#{
	return minRange + (sampled * (maxRange - minRange))
#}

func ComputeEnergy(sampled) -> float:
#{
	return minEnergy + (sampled * (maxEnergy - minEnergy))
#}

func ComputeSample(delta):
#{
	sample += delta * speed
	
	if sample >= 100000: sample = 0
#}

func SampleNoise() -> float:
#{
	return abs(noise.get_noise_1d(sample))
#}

func InitNoise():
#{
	noise.seed = randi()
	noise.octaves = 1
	noise.period = 1
	noise.lacunarity = .8
#}
