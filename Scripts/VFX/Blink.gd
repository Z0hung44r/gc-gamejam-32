extends Node

export var delay = 1
var timer = delay
var active = false

func _process(delta):
#{
	if not active: 
		return
	
	timer -= delta
	
	if timer <= 0:
		self.visible = not self.visible
		timer = delay
#}

func SetActive(isActive):
	active = isActive
