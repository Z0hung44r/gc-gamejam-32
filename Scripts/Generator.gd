extends MeshInstance

func _ready():
	SetState(true)

func SetState(state):
	self.get_node("LeverPanel/FeedbackON").visible = state
	self.get_node("LeverPanel/LeverON").visible = state
	self.get_node("LeverPanel/FeedbackOFF").visible = not state
	self.get_node("LeverPanel/LeverOFF").visible = not state
	
	self.get_node("LeverPanel/FeedbackON").SetActive(state)	
	self.get_node("LeverPanel/FeedbackOFF").SetActive(not state)	
