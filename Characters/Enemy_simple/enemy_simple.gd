extends "res://Characters/Character.gd"

class_name enemy_simple

enum State {
	IDLE,
	IDLE2,
	IDLE3,
	LEFT,
	RIGHT,
	FRONT,
	BACK,
	CROUNCH,
	JUMP,
	ALERT,
	ATTACK,
	CHASE,
	DEAD #toujours le mettre a la fin pour ne pas qu'il soit dans le random
}

export(float) var patrolZone = 5
export(float) var gunDispersion = 0.25

var patroling = true
var state = null
var lastState = null
var timer = Timer
var timerDeath = 4
var seePlayer : bool
var timerSee : float
var timerDuration = 1
var changeState : bool

#on stack les coordonnées de spawn pour les recuperer pour la zone de patrouille
var spawnPoint = null
var zoneExited = null

# function pour un mouvement aléatoire
func randomState():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	return rng.randi_range(0,6)


func Move(delta):
	# distance entre le spawn point et le body
	zoneExited = self.translation.distance_to(spawnPoint)
	
	$Mesh/ThugV1/Gun00.global_transform.origin.y = 1.6
	
	match state:
		State.IDLE:
			$AnimationPlayer.play("ThugIdle")
			$Mesh/ThugV1/Gun00.global_transform.origin.y = 1.3
		State.IDLE2:
			$AnimationPlayer.play("ThugIdle")
			$Mesh/ThugV1/Gun00.global_transform.origin.y = 1.3
		State.IDLE3:
			$AnimationPlayer.play("ThugIdle")
			$Mesh/ThugV1/Gun00.global_transform.origin.y = 1.3
		State.FRONT:
			self.move_and_slide(Vector3(0,0,1))
			$Mesh/ThugV1.rotation_degrees = Vector3(0,-180,0)
			$AnimationPlayer.play("ThugWalkForward")
		State.BACK:
			self.move_and_slide(Vector3(0,0,-1))
			$Mesh/ThugV1.rotation_degrees = Vector3(0,0,0)
			$AnimationPlayer.play("ThugWalkForward")
		State.LEFT:
			self.move_and_slide(Vector3(-1,0,0))
			$Mesh/ThugV1.rotation_degrees = Vector3(0,90,0)
			$AnimationPlayer.play("ThugWalkForward")
		State.RIGHT:
			self.move_and_slide(Vector3(1,0,0))
			$Mesh/ThugV1.rotation_degrees = Vector3(0,-90,0)
			$AnimationPlayer.play("ThugWalkForward")
		State.ALERT:
			$Alert.visible = true
			$AnimationPlayer.play("ThugPistolAim")
			timerSee -= delta
			if timerSee <= 0:
				timerSee = 0
				state = State.IDLE
				seePlayer = false
				$Alert.visible = false
			
			self.look_at(Vector3(Util.playerPosition.x ,0.5,Util.playerPosition.z), Vector3(0,1,0))
			self.rotation_degrees += Vector3(0,180,0)
			
			$Mesh/ThugV1/Gun00.Shoot($Mesh/ThugV1/Gun00/BulletPos.global_transform.origin, $Mesh/ThugV1/Gun00.global_transform.basis.z, -gunDispersion, gunDispersion, $Fire)
		#State.JUMP:
		#	velocity.y = 50
		#	state = State.IDLE
		#State.CHASE:
		#	var dir = (Util.playerPosition - global_transform.origin).normalized()
		#	move_and_slide(dir * 1 )
		#	$AnimationPlayer.play("ThugWalkForward")
		#State.ATTACK:
		#	var dir = (Util.playerPosition - global_transform.origin).normalized()
			
		#	$AnimationPlayer.play("ThugPistolAim")
		#	$Mesh/ThugV1/Gun00.Shoot($Mesh/ThugV1/Gun00/BulletPos.global_transform.origin, -$Mesh/ThugV1/Gun00.global_transform.basis.z, -gunDispersion, gunDispersion)
		#	#$Mesh/RayCast.look_at(Vector3(Util.playerPosition.x ,0,Util.playerPosition.z), Vector3(0,1,0))
		#	#$Mesh/RayCast.rotation_degrees += Vector3(0,180,0)
		State.DEAD:
			$Alert.visible = false
			timerDeath -= delta
			if timerDeath <= 0:
				queue_free()

func MoveFPS(delta):
	Move(delta)

func MoveTopView(delta):
	Move(delta)

func MovePlatformer(delta):
	Move(delta)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	#stockage des informations du spawn
	spawnPoint = self.translation
	$AnimationPlayer.play("ThugIdle")
	seePlayer = false
	timerSee = timerDuration
	changeState = false
	state = State.IDLE

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if seePlayer and state != State.DEAD:
		self.state = State.ALERT
	
	if Util.nightVisionActive:
		$Mesh/ThugV1/FieldOfView.scale = Vector3(0.5, 0.5, 0.5)
	else:
		$Mesh/ThugV1/FieldOfView.scale = Vector3(1, 1, 1)


func _on_Timer_timeout():
	if state == State.DEAD: return
	
	if patroling and zoneExited < patrolZone:
		var s = randomState()
		state = s
		lastState = s
	else:
		if lastState == State.RIGHT:
			state=State.LEFT
		if lastState == State.LEFT:
			state=State.RIGHT
		if lastState == State.FRONT:
			state=State.BACK
		if lastState == State.BACK:
			state=State.FRONT


func _on_Bullet_area_entered(body):
	if body is Bullet:
		state = State.DEAD
		$AnimationPlayer.play("ThugDeath")
		$Mesh/ThugV1/Gun00.visible = false
		$Death.play()
		body.queue_free()
