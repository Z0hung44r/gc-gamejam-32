extends RayCast

func _physics_process(delta):
	var body = self.get_collider()
	if body != null:
		if body.name == "Player":
			var rootNode = self.get_parent().get_parent().get_parent().get_parent().get_parent()
			rootNode.seePlayer = true
			rootNode.timerSee = rootNode.timerDuration
