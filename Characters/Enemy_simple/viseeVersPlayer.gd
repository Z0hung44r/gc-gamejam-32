extends RayCast

var timeout = false

func _physics_process(delta):
	var body = self.get_collider()
	var enemyNode = self.get_parent().get_parent().get_parent()
	if body != null:
		if body is StaticBody and not enemyNode.changeState:
			if enemyNode.state == enemyNode.State.FRONT:
				enemyNode.state = enemyNode.State.BACK
			elif enemyNode.state == enemyNode.State.BACK:
				enemyNode.state = enemyNode.State.FRONT
			elif enemyNode.state == enemyNode.State.RIGHT:
				enemyNode.state = enemyNode.State.LEFT
			elif enemyNode.state == enemyNode.State.LEFT:
				enemyNode.state = enemyNode.State.RIGHT
			enemyNode.changeState = true
	else:
		enemyNode.changeState = false
