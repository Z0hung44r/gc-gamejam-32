extends KinematicBody

#gravité
var velocity = Vector3.ZERO
var gravity = 0.65

# Déplacement du personnage en vue FPS
func MoveFPS(delta):
	pass

func MoveTopView(delta):
	pass

func MovePlatformer(delta):
	pass

func _ready():
	velocity = Vector3.ZERO
	
func _physics_process(delta):
	if not is_on_floor():
		velocity.y -= gravity
		velocity = move_and_slide(velocity, Vector3.UP)
	else:
		velocity.y = -0.2
	
	if Util.currentView == Util.PointOfView.PLATFORMER:
		MovePlatformer(delta)
	elif Util.currentView == Util.PointOfView.TOP_VIEW:
		MoveTopView(delta)
	elif Util.currentView == Util.PointOfView.FPS:
		MoveFPS(delta)
	
