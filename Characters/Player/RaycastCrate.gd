extends RayCast

#export (float) var pushSpeed = 0.5

var collideWithCrate : bool
var currentCrate : KinematicBody
var regex : RegEx

func _ready():
	collideWithCrate = false
	currentCrate = null
	regex = RegEx.new()
	regex.compile("Crate[0-9][0-9]")

func _physics_process(delta):
	if (Util.currentView != Util.PointOfView.PLATFORMER): return
	
	var body = self.get_collider()
	if body != null:
		if body is KinematicBody and regex.search(body.name) and Input.is_action_pressed("ui_interact"):
			collideWithCrate = true
			currentCrate = body
	else:
		collideWithCrate = false
	
	if collideWithCrate:
		var playerNode = self.get_parent().get_parent()
		playerNode.pushCrate = true
		currentCrate.move_and_slide(Vector3(0, 0, playerNode.moveSpeed), Vector3.UP)
		playerNode.move_and_slide(Vector3(0, 0, playerNode.moveSpeed), Vector3.UP)
	
	if Input.is_action_just_released("ui_interact"):
		collideWithCrate = false
		var playerNode = self.get_parent().get_parent()
		playerNode.pushCrate = false
