extends RayCast

func _physics_process(delta):
	if Util.currentView != Util.PointOfView.FPS: return
	
	var body = self.get_collider()
	if body != null:
		if body.name == "LeverBody" and Input.is_action_just_pressed("ui_interact"):
			var LevelNode = get_tree().get_root().get_node("Level1")
			var generatorNode = LevelNode.get_node("Gameplay").get_node("Road").get_node("Objects")
			var lightsNode = LevelNode.get_node("Scenery").get_node("Lights")
			var vfxNode = LevelNode.get_node("Scenery").get_node("VFX")
			var lightsLeftNode = LevelNode.get_node("Gameplay").get_node("Road").get_node("LeftSide")
			var lightsRightNode = LevelNode.get_node("Gameplay").get_node("Road").get_node("RightSide")
			var lightPlayer = LevelNode.get_node("Player").get_node("CameraFPS").get_node("LightFill")
			
			generatorNode.get_node("Generator").SetState(Util.nightVisionActive)
			generatorNode.get_node("Generator2").SetState(Util.nightVisionActive)
			generatorNode.get_node("Generator3").SetState(Util.nightVisionActive)
			
			lightsNode.visible = Util.nightVisionActive
			vfxNode.visible = Util.nightVisionActive
			lightsLeftNode.visible = Util.nightVisionActive
			lightsRightNode.visible = Util.nightVisionActive
			lightPlayer.visible = Util.nightVisionActive
			Util.nightVisionActive = not Util.nightVisionActive
