extends "res://Characters/Character.gd"

class_name Player #Important pour le spawn du player

enum Direction {
	UP,
	DOWN
}

export(float) var moveSpeed = 5.0 # Vitesse de déplacement du joueur
export(float) var angularAcceleration = 15 # Vitesse de rotation du joueur
export(float) var SpeedNightVision = 0.5 # Temps avant l'activation de la vision nocturne en secondes
export(float) var jump = 8


#var velocity : Vector3
var currentCamera : Camera # Caméra utilisé actuellement
var basisFPS : Basis # Basis utilisé pour la vue FPS

var strafe_dir : Vector3
var strafe : Vector3
var currentDir
var timer : float
var nightVisionActive : bool
var pushCrate : bool
var timerBeforeShoot = 0.5

func MoveTopView(delta):
	var meshOrientation = Vector3(0, $Mesh.rotation_degrees.y, 0)
	velocity = Vector3(0, velocity.y, 0)
	
	if Input.is_action_pressed("ui_left"):
		velocity.x += moveSpeed
	if Input.is_action_pressed("ui_right"):
		velocity.x -= moveSpeed
	if Input.is_action_pressed("ui_up"):
		velocity.z += moveSpeed
		currentDir = Direction.UP
	if Input.is_action_pressed("ui_down"): 
		velocity.z -= moveSpeed
		currentDir = Direction.DOWN
	
	if currentDir == Direction.UP:
		meshOrientation.y = 0
	elif currentDir == Direction.DOWN:
		meshOrientation.y = 180
	
	$Mesh.rotation_degrees.y = lerp($Mesh.rotation_degrees.y, meshOrientation.y, angularAcceleration * delta)
	strafe_dir = velocity
	velocity.normalized()
	move_and_slide(velocity, Vector3.UP)

func MoveFPS(delta):
	velocity = Vector3(0, velocity.y, 0)
	var direction = Vector3.ZERO
	
	if Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_down"):
		var hCamRotation = $CameraFPS.global_transform.basis.get_euler().y
		
		direction = Vector3(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
							0,
							Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")).rotated(Vector3.UP, hCamRotation).normalized()

	move_and_slide(direction * moveSpeed, Vector3.UP)

func MovePlatformer(delta):
	velocity = Vector3(0, velocity.y, 0)
	
	if Input.is_action_just_pressed("ui_jump") and is_on_floor():
		velocity.y += jump
		$Jump.play()
	
	$Mesh/Gun.global_transform.origin.y = 1.25 + self.translation.y
	
	if Input.is_action_pressed("ui_up"):
		velocity.x += moveSpeed
	if Input.is_action_pressed("ui_down"):
		velocity.x -= moveSpeed
	if Input.is_action_pressed("ui_left"):
		velocity.z -= moveSpeed
		$Mesh/Gun.global_transform.origin.y = 1.5 + self.translation.y
	if Input.is_action_pressed("ui_right"):
		velocity.z += moveSpeed
		$Mesh/Gun.global_transform.origin.y = 1.5 + self.translation.y
	
	strafe_dir = velocity
	velocity.normalized()
	velocity = move_and_slide(velocity, Vector3.UP)

func ChangeCamera(pNewCamera):
	Util.oldView = Util.currentView
	currentCamera.current = false
	if Util.nightVisionActive:
		currentCamera.get_node("NightVision").visible = false
	currentCamera = pNewCamera
	pNewCamera.current = true

func SetBasis(pNewPOV):
	if pNewPOV == Util.oldView: return
	
	if pNewPOV != Util.PointOfView.FPS:
		if Util.oldView == Util.PointOfView.FPS:
			basisFPS = global_transform.basis
		
		# Corrige l'orientation de la caméra
		global_transform.basis.x = Vector3.RIGHT
		global_transform.basis.y = Vector3.UP
		global_transform.basis.z = Vector3.BACK
	else:
		global_transform.basis = basisFPS

# Called when the node enters the scene tree for the first time.
func _ready():
	
	velocity = Vector3.ZERO
	currentCamera = $CameraFPS
	$Mesh.visible = false
	
	strafe_dir = Vector3.ZERO
	strafe = Vector3.ZERO
	timer = 0
	nightVisionActive = false
	pushCrate = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Util.nightVisionActive and timer <= 0 and !nightVisionActive:
		timer = SpeedNightVision
	elif !Util.nightVisionActive:
		nightVisionActive = false
	#avoir la position du joueur
	Util.playerPosition = self.global_transform.origin
	
	if timer > 0:
		timer -= delta
		if timer <= 0:
			nightVisionActive = true
	
	currentCamera.get_node("NightVision").visible = nightVisionActive
	
	# Changement de point de vue
	if Input.is_action_just_pressed("ui_pov_fps"):
		ChangeCamera($CameraFPS)
		Util.currentView = Util.PointOfView.FPS
		SetBasis(Util.currentView)
		$Mesh.visible = false
		$CameraFPS/MeshFPS.visible = true
	elif Input.is_action_just_pressed("ui_pov_platformer"):
		ChangeCamera($CameraPlatformer)
		Util.currentView = Util.PointOfView.PLATFORMER
		SetBasis(Util.currentView)
		$Mesh.visible = true
		$Mesh.rotation_degrees.y = 0
		$CameraFPS/MeshFPS.visible = false
	elif Input.is_action_just_pressed("ui_pov_top_view"):
		ChangeCamera($CameraTopView)
		Util.currentView = Util.PointOfView.TOP_VIEW
		SetBasis(Util.currentView)
		$Mesh.visible = true
		currentDir = Direction.UP
		$CameraFPS/MeshFPS.visible = false
	
	if Input.is_action_just_pressed("ui_shoot"):
		var bulletPosNode
		if Util.currentView == Util.PointOfView.FPS:
			bulletPosNode = $CameraFPS/MeshFPS/Gun/BulletPos
			$CameraFPS/MeshFPS/Gun.Shoot(bulletPosNode.global_transform.origin, $CameraFPS.global_transform.basis.z, -0.2, 0.2, $Fire)
		else:
			bulletPosNode = $Mesh/Gun/BulletPos
			$Mesh/Gun.Shoot(bulletPosNode.global_transform.origin, -$Mesh.global_transform.basis.z, -0.2, 0.2, $Fire)
	
	if Util.currentView == Util.PointOfView.FPS:
		$CameraFPS.Move(delta)
	else:
		if velocity.y >= 0.25:
			$AnimationTreeV2.set("parameters/JumpTransition/current", 2)
		elif pushCrate:
			$AnimationTreeV2.set("parameters/JumpTransition/current", 4)
		else:
			strafe = lerp(strafe, strafe_dir, delta * 5)
			$AnimationTreeV2.set("parameters/JumpTransition/current", 0)
			$AnimationTreeV2.set("parameters/BlendSpace2D/blend_position", Vector2(strafe.x, strafe.z))
