extends Area

class_name Bullet

export (float) var lifeTime = 3 # Temps de survie d'une balle (en secondes)
export (float) var speed = 1

onready var SceneGameOver = load("res://Scenes/Build/DeadScene/DeadScene.tscn")

var direction : Vector3

func _process(delta):
	self.lifeTime -= delta
	if self.lifeTime <= 0:
		queue_free()
	
	self.transform.origin -= direction * speed * delta

func _on_Bullet_body_entered(body):
	if body is StaticBody:
		queue_free()
	elif body is Player:
		get_tree().change_scene_to(SceneGameOver)
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		Util.moveCamera = false
