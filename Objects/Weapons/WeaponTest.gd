extends Spatial

export (float) var RateOfFire = 60# Cadence de tir (en nombre de tir/minute)
export (int) var NbBulletPerShoot = 1 # Nombre de balle tiré par coup de feu

const Bullet = preload("res://Objects/Bullet/Bullet.tscn")

var timerFire : float
var speedFire : float
var readyToFire : bool
var rng : RandomNumberGenerator

func Shoot(pPosition, pDirection, pVariationMin, pVariationMax, pSound):
	if not readyToFire: return
	for i in range(NbBulletPerShoot):
		var bullet = Bullet.instance()
		bullet.speed = 10
		bullet.direction = Vector3( pDirection.x + rng.randf_range(pVariationMin, pVariationMax), 
									pDirection.y + rng.randf_range(pVariationMin, pVariationMax), 
									pDirection.z + rng.randf_range(pVariationMin, pVariationMax)
						)
		bullet.direction.normalized()
		bullet.global_transform.origin = pPosition
		var rootNode = get_tree().get_root().get_child(0)
		rootNode.add_child(bullet)
	
	timerFire = speedFire
	readyToFire = false
	pSound.play()

func _ready():
	speedFire = (1/RateOfFire)*60
	timerFire = 1
	readyToFire = false
	
	rng = RandomNumberGenerator.new()
	rng.randomize()


func _process(delta):
	if timerFire >= 0:
		timerFire -= delta
	else:
		readyToFire = true
